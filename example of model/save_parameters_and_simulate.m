% this file saves the paramters provided by the students and simulates the system.
% it also contains parameters that should not be changed. 

vehicle_weight = 1730;
tc_F_req_real = 1.18;   % time constant between requested and current engine force
Ta = 2e-3;              % time constant of differentiator
ped_to_F = 21000/10;% 1587.66/100; % pedal to engine force conversion

if Ti == 0
    Ti = 1e-9;
end

filename = 'simulation_parameters';
save(filename, 'ref_speed',  'driver_pedal_position', 'Tend', 'Ts', 'initial_vehicle_speed', ...
    'Ti', 'Ta', 'Td', 'K', ...
    'vehicle_weight', 'tc_F_req_real', 'ped_to_F', ...
    '-v7.3');

addpath(pwd);
simulation = sim('cruise_control.slx');
