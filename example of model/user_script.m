clear all; close all; clc

Ts          = 1/100;                            % sampling time of signals
Tend        = 500;                               % length in seconds of simulation
Tstep       = 5;                                % time at which step in reference speed is applied
time        = 0:Ts:Tend;                        % time axis of input
step_sign   = 20*heaviside(time-Tstep);            % this is like a step function

ref_speed               = timeseries(step_sign, time);      % store it into the input timeseries object
driver_pedal_position   = timeseries(step_sign, time);

initial_vehicle_speed = 90;

% PID controller
Ti  = 1e9;
Td  = 0;
K   = 1;

save_parameters_and_simulate;

%% plot section
lw = 1.5;
figure()
plot(simulation.requested_speed, 'DisplayName', 'requested speed', 'linewidth', lw);
hold on, grid on
plot(simulation.output_speed, 'DisplayName', 'output', 'linewidth', lw);
hold on, grid on, 
plot(simulation.pedal_position, 'DisplayName', 'pedal position', 'linewidth', lw);
legend()
ylabel('signal values')
xlabel('time [seconds]')



